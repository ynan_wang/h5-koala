const gulp = require('gulp')
const sass = require('gulp-sass')
const autoprefixer = require('gulp-autoprefixer') // css加兼容的前缀
const through2 = require('through2')
const path = require('path')

function gulpSingle(file) {
    gulp.src(file.path)
        .pipe(sass().on('error', sass.logError))
        .pipe(
            autoprefixer({
                overrideBrowserslist: ['Android >= 4.0']
            })
        )
        .pipe(gulp.dest(path.dirname(file.path).replace('/src', '/lib')))
}

gulp.task('default', function() {
    return gulp
        .src(['./src/**/*.scss'])
        .pipe(
            through2.obj(function(file, encoding, next) {
                this.push(file.clone())
                if (file.path.match(/(\/|\\)(styles|src)(\/|\\)index\.scss$/)) {
                    gulpSingle(file)
                }
                next()
            })
        )
        .pipe(gulp.dest('./lib'))
})
