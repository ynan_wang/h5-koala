module.exports = {
    parser: "@typescript-eslint/parser",
    env: {
        es6: true,
        node: true,
        browser: true, 
    },
    extends: ['alloy', 'alloy/react','alloy/typescript',"eslint:recommended"],

    parserOptions: {
        ecmaFeatures: {
            experimentalObjectRestSpread: true,
            jsx: true
        },
        useJSXTextNode: true,
        ecmaVersion: 10, // 为了使用  async、await
        sourceType: "module"
    },
    plugins: [
        'react-hooks'
    ],
    rules: {
        'complexity':0,
        "no-param-reassign": 0,
        "array-callback-return": 0,
        "default-case": 0,
        "default-case-last": 0,
        "no-constructor-return": 0,
        "no-dupe-else-if": 0,
        "no-setter-return": 0,
        "no-useless-backreference": 0,
        "grouped-accessor-pairs": 0,
        "no-control-regex": "off",
        "no-debugger": 0, // 禁止使用debugger
        "no-unused-vars": ["warn", { vars: "all", args: "after-used", ignoreRestSiblings: false }], // 声明未使用的变量 警告
        "no-console": "warn",
        "react/jsx-uses-vars": ["error"],
        "no-case-declarations": "off",
        "no-useless-escape": "off",
        "react-hooks/rules-of-hooks": "error", // 检查 Hook 的规则
        eqeqeq: ["warn", "allow-null"], // == ===
        "react-hooks/exhaustive-deps": "off",// 这里如果打开prettier会自动去改dep
        "no-var": "warn",
        "max-params": ["warn", 6],
        "no-inner-declarations":'off',
        "no-useless-concat":'off',
        "no-irregular-whitespace":'off',
        "guard-for-in":'off',
         /**
         * 不校验类的成员的可访问性
         * @reason 自带生命周期方法写可访问性较累赘
         */
        '@typescript-eslint/explicit-member-accessibility': 'off',
        /**
         * 可以使用 require
         * @reason 要使用懒加载等
         */
        '@typescript-eslint/no-require-imports': 'off',
        /**
         * 类型断言必须使用 as Type，禁止使用 <Type>
         * @reason <Type> 容易被理解为 jsx
         */
        '@typescript-eslint/consistent-type-assertions': [
            'error',
            {
                assertionStyle: 'as',
                objectLiteralTypeAssertions: 'allow'
            }
        ],
        '@typescript-eslint/prefer-optional-chain':'off',
        '@typescript-eslint/no-unused-expressions':'off',
        '@typescript-eslint/prefer-for-of':'off',
        "@typescript-eslint/no-invalid-this":'off'

    }
};


