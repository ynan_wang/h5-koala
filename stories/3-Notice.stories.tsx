import React from 'react'
import {TextNotice,CardNotice} from '../src/components/notice'
import '../src/components/notice/styles/index.scss'
export default {
    title: 'Notice',
  };

const messageArray1=[{message:'本文本文文本文本文文本本文本文本文本文本文文本本文本文本文本文本文文本本文本文本文本文本文文本本文本文本文本文本文文本本文本文本文本文本文文本本文本文本文本文本文文本本文本文本文本文本文文本本文本文本文本文本文文本本文本文本文本文本文文本本文本文本文本文本文文本本文本文本文本文本文文本本文本文本文本文本文文本本文本文本文本文本文文本文本文本本文本文文本文本文本本文本文文本文本文本本文本文文本文本文本本文本文文本文本文本本文本文',onShowDetail:()=>alert("点击详情"),
}]

const messageArray2=[{message:'本文本文文本文本文文本本文本文本文本文本文文本本文本文本文本文本文文本本文本文本文本文本文文本本文本文本文本文本文文本本文本文本文本文本文文本本文本文本文本文本文文本本文本文本文本文本文文本本文本文本文本文本文文本本文本文本文本文本文文本本文本文本文本文本文文本本文本文本文本文本文文本本文本文本文本文本文文本本文本文本文本文本文文本文本文本本文本文文本文本文本本文本文文本文本文本本文本文文本文本文本本文本文文本文本文本本文本文'}
]
const messageArray3=[{message:'文文本本文本本文本文本文本本文本本本文本文',detailedText:"功能操作",onShowDetail:()=>alert("点击功能操作")}]
const messageArrayMore=[{message:'本文本文本本文本文本。文本文本。本文本文本本文本文本。文本文本。本文本文本本文本文本。文本文本。',onShowDetail:()=>alert("点击详情")},
{message:'文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本'},
{message:'文本文本文本文本文本文本文本',detailedText:"详情",onShowDetail:()=>alert("点击详情"),isLimitRow:false}]
const button={title:'次要按钮',onClick:()=>{alert('点击次要按钮')}}
export const textNotice=()=>{
    return<div>
    {/* <div>
    <TextNotice color ='#666666' messageArray={messageArray2} leftIconType="Dot" padding={{left:"50px",right:"30px"}}/><br/>
    </div> */}
    <div>
    <TextNotice color ='#666666' messageArray={messageArray3} /><br/>
    </div>
    <div>
    <TextNotice color ='#666666' messageArray={messageArray2} leftIconType="Information" /><br/>
    </div>
    <div>
    <TextNotice color ='#666666' messageArray={messageArray1} leftIconType="Information" /><br/>
    </div>
    <div>
    <TextNotice color ='#666666' messageArray={messageArray1} leftIconType="Information" onClose={()=>alert('点击关闭按钮')}/><br/>
    </div>
    <div>
    <TextNotice color ='#666666' messageArray={messageArray2} button={button}/>
    </div>
    <div>
    <TextNotice color ='#666666' messageArray={messageArray2} button={button} onClose={()=>alert('点击关闭按钮')} leftIconType="Dot"/><br/>
    </div>
    </div>

}
export const textNoticeMore=()=>{
    return<TextNotice color ='#FF7700' messageArray={messageArrayMore} onClose={()=>alert('点击关闭按钮')} leftIconType="Information" backgroundColor="#FFF8F2"/>

}


const title="资源变动提示"
const btns=[{title:'按钮文字最多十八个字',focus:false,onClick:()=>alert('点击按钮')},
{title:'焦点按钮强提示',focus:true,onClick:()=>alert('点击按钮')}]

export const cardNotice=()=>{
    return<div>
        <CardNotice title={title} btns={btns} /><br/>
        <CardNotice title={title} btns={btns} btnDirection="row"/>
        </div>
}

const color="#FF7700";
const messageArrayPaneL=[{message:"提示话术提示话术提示话术提示话话术提示话术"},
{message:"提示话术提示话术提示话术提示话话术提示话术提示话术提示话术提示话术提示话话术提示话术提示话术提示话术提示话术提示话话术提示话术"},
{message:"提示话术提示话术提示话术提示话术提示话术提示话术提示话术提示话术提示话术提示话术提示话术提示话术提示话术提示话术提示话术提示话术提示话术提示话术",onClose:()=>alert('关闭按钮')},
{message:"提示话术提示话术提示话术提示话术提示话术提示话术提示话术提示话术提示话术提示话术提示话术提示话术提示话术提示话术提示话术提示话术提示话术提示话术",onClose:()=>alert('关闭按钮'),button:{title:"一键替换",onClick:()=>alert('一键替换')}},
{message:"提示话术提示话术提示话术提示话术提示话术提示话术提示话术提示话术提示话术",button:{title:"一键替换",onClick:()=>alert('一键替换')}}]


