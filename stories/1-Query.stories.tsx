import React from 'react'
import { commonInfoConfig } from '../src/mock/commoninfoConfig'
import { QueryForm } from  '../src/index'
import { Form } from 'antd'
export default {
    title: ' QueryConditions'
  };
export const QueryConditionsTest = () => {
    const search = () => {
        queryForm.validateFields().then(() => {
            console.log(queryForm.getFieldsValue())
        })
        
    }

  const [queryForm] = Form.useForm()

    return (
        <QueryForm 
            queryForm = {queryForm}
            colNumber = {3}
            queryConfig = {commonInfoConfig}
            formStyle = {{marginTop:20,marginLeft:40}}
            selectStyle = {{width:150}}
            inputStyle = {{width:150}}
            labelAlign = 'right'
        />
    )
}