## Form表单

### 使用

```js
import { QueryForm } from 'h5-koala'
import { Form } from 'antd'
const commonInfoConfig = [
    {
        label: '业务线信息',
        type: 'select',
        key: 'appList',
        isRequire: true,
        isShowLabel: true,
        tooltip: '哈哈哈哈哈, 很高兴遇到你',
        tooltipIcon: <InfoCircleOutlined />,
        col: 6
    },
    {
        label: '上报终端类',
        type: 'checkbox',
        isRequire: true,
        isShowLabel: true,
        key: 'platform',
        col: 24,
        data: [
            { label: 'Apple', value: 'Apple' },
            { label: 'Pear', value: 'Pear' },
            { label: 'Orange', value: 'Orange' },
            { label: 'Apple1', value: 'Apple1' },
        ]
    },

    {
        key: 'action',
        name: '按钮',
        type: 'button',
        text: '添加参数',
        antdOptions: {
            type: 'primary'
        },
        col: 20
    }
]
export const QueryConditionsTest = () => {
    const [queryForm] = Form.useForm()
    const search = () => {
        queryForm.validateFields().then(() => {
            console.log(queryForm.getFieldsValue())
        })
        
    }

    return (
        <QueryForm 
            queryForm = {queryForm}
            colNumber = {3}
            queryConfig = {commonInfoConfig}
            formStyle = {{marginTop:20,marginLeft:40}}
            selectStyle = {{width:150}}
            inputStyle = {{width:150}}
            labelAlign = 'right'
        />
    )
}
```

### 组件参数说明

| 参数名称        | 是否必填          | 说明 | 默认值  |
| ------------- |:-------------:|:-------:| -----:|
|queryForm |必填|form实例|null
|colNumber |非必填  | col与colNumber必须二填一|0
|queryConfig|必填| 组件配置对象|[]
|formStyle|非必填| 组件最外层样式|{}
|selectStyle|非必填|select组件样式|{}
|inputStyle|非必填|input组件样式|{}
|textAreaStyle|非必填|textArea组件样式|{}
|buttonStyle|非必填|button组件样式|{}
|checkBoxStyle|非必填|checkbox组件样式|{}
|radioGroupStyle|非必填|radioGroup组件样式|{}
|innerItemStyle|非必填| 暂无|暂无
|labelAlign|非必填| Form.item label标签对齐方式|left
|labelCol|非必填|Form.item label标签的col|null


### 配置文件参数说明

| 参数名称        | 是否必填          | 说明 | 默认值  |
| ------------- |:-------------:|:-------:| -----:|
|data|非必填|select下拉框的option|[]
|col|col与colNumber必须二填一|该item占据col|
|key|必填|item唯一标识|''
|isShowLabel|非必填|是否展示标签|false
|label|非必填|标签文案|''
|style|非必填|item样式
|tooltip|非必填|formItem tooltip|undefined
|tooltipIcon|非必填|formItem tooltip icon|undefined
|isRequire|非必填|是否使用rules|undefined
|definedRule|非必填|自定义rules规则(isRequire必须为true)|undefined
|type|必填|formItem 类型（select等）|
|antdOptions|非必填|item的antd属性|undefined
|search|非必填|button的onclick函数|undefined
|text|非必填|button文案|undefined
|keyCode|非必填|button配置快捷键|undefined


## 示例

![](./image/query.jpeg#wh3)