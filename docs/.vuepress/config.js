const path = require('path');
const moment = require('moment')

module.exports = {
  base: '/h5-koala/',
  dest: 'public',
  title: 'h5-koala 使用指南',
  description: 'react UI 组件库',
  // 注入到当前页面的 HTML <head> 中的标签
  head: [
    ['link', { rel: 'icon', href: '/koala.ico' }]
  ],
  // 站点多语言配置
  locales: {
    '/': {
      // 将会被设置为 <html> 的 lang 属性 会对格式化日期有影响
      lang: 'zh-CN',
    },
  },
  // webpack配置
  configureWebpack: {
    resolve: {
      alias: {
        '@public': path.resolve(__dirname, './public')
      }
    }
  },
  // markdown配置
  markdown: {
    lineNumbers: false, // 代码块显示行号
  },
  themeConfig: {
    // Gitlab URL
    repo: 'https://gitlab.com/ynan_wang/h5-koala.git',
    // 自定义仓库链接文字
    repoLabel: 'gitlab',
    // 文档仓库地址
    docsRepo: 'https://ynan_wang.gitlab.io/h5-koala/',
    // 文档分支：
    docsBranch: 'master',
    nav: [
      { text: '博客', link: 'https://blog.csdn.net/kellywong/article/details/107499153' },
      { text: 'npm', link: 'https://www.npmjs.com/package/h5-koala' },
    ],
    // 假如文档不是放在仓库的根目录下
    docsDir: 'docs',
    // 默认是 false, 设置为 true 来启用
    editLinks: true,
    // 默认为 "Edit this page"
    editLinkText: '在 GitLab 上编辑此页',
    // 平滑滚动
    smoothScroll: true,
    // Service Worker
    serviceWorker: {
      // 刷新内容的弹窗
      updatePopup: true,
    },
    // 最后更新时间 string | boolean => 最后更新时间: 7/10/2019, 1:17:38 PM
    lastUpdated: '上次更新',
    // 侧边栏
    sidebar: [
      {
        title: '如何开始',
        collapsable: false,
        children: [
          'quick_start/',
        ]
      },
      {
        title: '组件使用',
        collapsable: false,
        children: [
          'component/color',
          'component/queryForm',
        ]
      },
    ],
  },
  // 插件
  plugins: [
    ['@vuepress/back-to-top', true],
    'vuepress-plugin-mermaidjs',
    [
      '@vuepress/last-updated',
      {
        transformer: (timestamp, lang) => {
          moment.locale(lang)
          return moment(timestamp).format('YYYY-MM-DD HH:mm:ss')
        }
      }
    ]
  ],
};
