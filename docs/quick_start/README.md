# 快速接入

## 安装

```bash
npm install h5-koala
```

## 使用

```js
import { Button } from 'h5-koala'
```

## 说明

该UI组件依赖antd4.0以上版本,react 16.8版本

