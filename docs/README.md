---
home: true
heroImage: /image/koala.png
actionText: 快速上手 →
actionLink: /quick_start/
footer: MIT Licensed | Copyright © kellywang 2020
features:
  - title: h5组件库
    details: 这个组件非常非常好使
  - title: react驱动
    details: 基于react
  - title: 非常好使
    details: 这个组件非常非常好使
---
