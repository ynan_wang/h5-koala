"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.KColor = exports.gradientColors = exports.defaultColors = exports.colors = void 0;
const black = {
    black: '#000000'
};
const white = {
    white: '#FFFFFF' // 白色
};
const blue = {
    blue01: '#0072D1',
    blue02: '#007FE9',
    blue03: '#0086F6',
    blue04: '#2698F7',
    blue05: '#4DAAF8',
    blue06: '#73BCFA',
    blue07: '#99CEFB',
    blue08: '#BFE0FC',
    blue09: '#E6F3FE',
    blue10: '#F2F8FE' // 浅色衬底
};
const green = {
    // 辅助色 - 绿（有益、正向良性、成功）
    green01: '#009C67',
    green02: '#00AE73',
    green03: '#00B87A',
    green04: '#26C28D',
    green05: '#4DCDA2',
    green06: '#73D8B5',
    green07: '#99E2C9',
    green08: '#BFEDDD',
    green09: '#E6F8F1',
    green10: '#F2FBF8' // 浅色衬底
};
const orange = {
    // 辅助色 - 橙（提醒、强调、营销激励）
    orangeS: '#FF6600',
    orange01: '#D96500',
    orange02: '#F27000',
    orange03: '#FF7700',
    orange04: '#FF8B26',
    orange05: '#FFA04D',
    orange06: '#FFB473',
    orange07: '#FFC899',
    orange08: '#FFDCBF',
    orange09: '#FFF1E6',
    orange10: '#FFF8F2' // 浅色衬底
};
const red = {
    // 辅助色 - 红（报错、警示、强激励）
    red01: '#D01508',
    red02: '#E81709',
    red03: '#F5190A',
    red04: '#F63B2E',
    red05: '#F85E53',
    red06: '#F98078',
    red07: '#FBA39D',
    red08: '#FCC5C1',
    red09: '#FEE8E6',
    red10: '#FEF3F2' // 浅色衬底
};
const gray = {
    // 灰度色 - 灰
    gray01: '#333333',
    gray02: '#666666',
    gray03: '#999999',
    gray04: '#AAAAAA',
    gray05: '#BBBBBB',
    gray06: '#CCCCCC',
    gray07: '#DDDDDD',
    gray08: '#EEEEEE',
    gray09: '#F4F4F4',
    gray10: '#F8F8F8' // App背景色
};
const blueGray = {
    // 灰度色 - 蓝灰
    blueGray01: '#5678A8',
    blueGray02: '#89A0C2',
    blueGray03: '#99AECA',
    blueGray04: '#AABBD3',
    blueGray05: '#BBC9DC',
    blueGray06: '#CCD6E5',
    blueGray07: '#DDE4ED',
    blueGray08: '#EEF1F6',
    blueGray09: '#F6F8FA',
    blueGray10: '#F8FAFD'
};
const yellow = {
    // 其他色 - 黄
    yellow01: '#D8AD00',
    yellow02: '#F2C200',
    yellow03: '#FFCC00',
    yellow04: '#FED325',
    yellow05: '#FFDC4E',
    yellow06: '#FFE473',
    yellow07: '#FFEA97',
    yellow08: '#FFF2BE',
    yellow09: '#FFFAE6',
    yellow10: '#FFFCF3' // 浅色衬底
};
const purple = {
    // 其他色 - 紫
    purple01: '#6800D1',
    purple02: '#7400E9',
    purple03: '#7800F5',
    purple04: '#8E26F7',
    purple05: '#A34DF8',
    purple06: '#B573F9',
    purple07: '#C898FC',
    purple08: '#DDBFFB',
    purple09: '#F2E6FC',
    purple10: '#F8F2FE' // 浅色衬底
};
const gold = {
    goldText: '#B37E36',
    goldBorder: '#E8DEC2' // 金色边框
};
const colors = {
    white,
    blue,
    green,
    orange,
    red,
    gray,
    blueGray,
    yellow,
    purple,
    gold,
    black
};
exports.colors = colors;
const defaultColors = Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign({}, white), blue), green), orange), red), gray), blueGray), yellow), purple), gold), black);
exports.defaultColors = defaultColors;
const gradientColors = {
    // 渐变色 - 蓝
    gBlue: ['#00A7FA', '#0076F5'],
    // 渐变色 - 橙
    gOrange: ['#FFA50A', '#FF7700'],
    // 渐变色 - 特殊橙（仅“订”按钮使用）
    gOrangeS: ['#FF9D0A', '#FF7700']
};
exports.gradientColors = gradientColors;
exports.KColor = Object.assign(Object.assign({}, defaultColors), gradientColors);
