import { defaultColors, gradientColors, colors, KColor } from './type';
export declare type IGradientColorIndex = keyof typeof gradientColors;
export declare type IGradientColorValue = typeof gradientColors[IGradientColorIndex];
export declare type IColorIndex = keyof typeof defaultColors;
export declare type IColorValue = typeof defaultColors[IColorIndex];
export declare type IColorsIndex = keyof typeof colors;
export declare type IKColorIndex = keyof typeof KColor;
export declare type IKColorValue = typeof KColor;
