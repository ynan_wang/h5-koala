"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __exportStar = (this && this.__exportStar) || function(m, exports) {
    for (var p in m) if (p !== "default" && !exports.hasOwnProperty(p)) __createBinding(exports, m, p);
};
Object.defineProperty(exports, "__esModule", { value: true });
__exportStar(require("./interface"), exports);
var type_1 = require("./type");
Object.defineProperty(exports, "colors", { enumerable: true, get: function () { return type_1.colors; } });
Object.defineProperty(exports, "gradientColors", { enumerable: true, get: function () { return type_1.gradientColors; } });
Object.defineProperty(exports, "KColor", { enumerable: true, get: function () { return type_1.KColor; } });
