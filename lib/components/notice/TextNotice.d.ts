import { Component } from 'react';
interface IProps {
    color: string;
    messageArray: {
        message: string;
        onShowDetail?: () => void;
        isLimitRow?: boolean;
        detailedText?: string;
    }[];
    onJump?: () => void;
    onClose?: () => void;
    button?: {
        title: string;
        onClick: () => void;
    };
    leftIconType?: 'Dot' | 'Information';
    backgroundColor?: string;
    padding?: {
        right?: string;
        left?: string;
        top?: string;
        bottom?: string;
    };
    margin?: {
        right?: string;
        left?: string;
        top?: string;
        bottom?: string;
    };
    borderRadius?: {
        topLeft?: string;
        topRight?: string;
        bottomLeft?: string;
        bottomRight?: string;
    };
}
interface IState {
    isShowArrow: boolean;
    isMoreLines: boolean;
}
export default class TextNotice extends Component<IProps, IState> {
    constructor(props: IProps);
    componentDidMount(): void;
    render(): JSX.Element;
}
export {};
