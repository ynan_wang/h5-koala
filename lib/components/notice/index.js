"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CardNotice = exports.TextNotice = void 0;
const TextNotice_1 = __importDefault(require("./TextNotice"));
exports.TextNotice = TextNotice_1.default;
const CardNotice_1 = __importDefault(require("./CardNotice"));
exports.CardNotice = CardNotice_1.default;
