"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importStar(require("react"));
const lodash_1 = __importDefault(require("lodash"));
class TextNotice extends react_1.Component {
    constructor(props) {
        super(props);
        this.state = {
            isShowArrow: false,
            isMoreLines: false // 有截断要求但是没超出行数
        };
    }
    componentDidMount() {
        const { messageArray = [] } = this.props;
        let isShowdetailed = lodash_1.default.some(messageArray, (message) => lodash_1.default.isFunction(message.onShowDetail));
        // 单文本，若有截断需求，只有超出三行才截断
        // 若为多文本，且有截断需求,单条超过一行就截断
        const koala_txt_arr = document.getElementsByClassName('koala_txt') || [];
        let textHeight = 0;
        let virtualHeight = 0;
        lodash_1.default.map(koala_txt_arr, (ele, index) => {
            if (index % 2 === 0) {
                textHeight += koala_txt_arr[index].clientHeight;
            }
            else {
                virtualHeight += koala_txt_arr[index].clientHeight;
            }
        });
        if (textHeight !== virtualHeight) {
            if (!isShowdetailed) {
                this.setState({ isShowArrow: true });
            }
            this.setState({ isMoreLines: true });
        }
    }
    render() {
        const { color = '#FF7700', messageArray = [], onJump, onClose, button = {}, leftIconType, backgroundColor = '#FFFFFF', padding = {}, margin = {}, borderRadius = {} } = this.props;
        let isMoreTips = false;
        if (messageArray.length > 1) {
            isMoreTips = true;
        }
        return (react_1.default.createElement("div", { className: "koala_ellipsisTxt", style: {
                color: color,
                backgroundColor: backgroundColor,
                paddingLeft: padding.left,
                paddingRight: padding.right,
                paddingTop: padding.top,
                paddingBottom: padding.bottom,
                marginLeft: margin.left,
                marginRight: margin.right,
                marginTop: margin.top,
                marginBottom: margin.bottom,
                borderTopLeftRadius: borderRadius.topLeft,
                borderTopRightRadius: borderRadius.topRight,
                borderBottomLeftRadius: borderRadius.bottomLeft,
                borderBottomRightRadius: borderRadius.bottomRight
            } },
            react_1.default.createElement("div", { className: "koala_tips_box" }, messageArray &&
                lodash_1.default.map(messageArray, (messageObj, index) => {
                    let { message = '', onShowDetail, isLimitRow = true, detailedText = '查看详情' } = messageObj;
                    let isShowDetail = false;
                    if (typeof onShowDetail === 'function') {
                        isShowDetail = true;
                    }
                    return (react_1.default.createElement("div", { key: index, className: "koala_risk_tips" },
                        leftIconType === 'Dot' && (react_1.default.createElement("i", { className: "koala_hasLeftPoint", style: { background: color } })),
                        leftIconType === 'Information' && (react_1.default.createElement("i", { className: "Koala_infoIcon" })),
                        react_1.default.createElement("p", { className: "koala_txt", style: {
                                WebkitLineClamp: isLimitRow
                                    ? isMoreTips
                                        ? 1
                                        : 3
                                    : 'none'
                            }, id: "textHeight" },
                            react_1.default.createElement("span", null, message),
                            !isLimitRow ||
                                (isLimitRow &&
                                    !this.state.isMoreLines &&
                                    isShowDetail && (react_1.default.createElement("span", { className: "koala_showDetail", onClick: onShowDetail }, detailedText)))),
                        react_1.default.createElement("p", { className: "koala_txt", style: { position: 'absolute', top: '-100000000px' }, id: "textHeightVirtual" },
                            react_1.default.createElement("span", null, message),
                            isMoreTips && isShowDetail && (react_1.default.createElement("span", { className: "koala_showDetail", onClick: onShowDetail }, detailedText))),
                        isLimitRow && this.state.isMoreLines && isShowDetail && (react_1.default.createElement("span", { className: "koala_showDetail_more", onClick: onShowDetail }, detailedText))));
                })),
            react_1.default.createElement("div", { className: "koala_operation" },
                !lodash_1.default.isEmpty(button) && (react_1.default.createElement("a", { className: "button", onClick: lodash_1.default.get(button, 'onClick'), style: { color: color } }, lodash_1.default.get(button, 'title'))),
                this.state.isShowArrow && lodash_1.default.isFunction(onJump) && (react_1.default.createElement("a", { className: "iconfont_jump", onClick: onJump, style: { color: color } })),
                lodash_1.default.isFunction(onClose) && (react_1.default.createElement("a", { className: "iconfont_close", onClick: onClose, style: { color: color } })))));
    }
}
exports.default = TextNotice;
