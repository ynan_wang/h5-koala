import { Component } from 'react';
interface IProps {
    title: string;
    btnDirection?: 'row' | 'column';
    btns: {
        title: string;
        focus: boolean;
        onClick: () => void;
    }[];
}
export default class CardNotice extends Component<IProps> {
    render(): JSX.Element;
}
export {};
