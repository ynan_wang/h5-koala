"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importStar(require("react"));
const lodash_1 = __importDefault(require("lodash"));
class CardNotice extends react_1.Component {
    render() {
        const { title = '', btnDirection = 'column', btns = [] } = this.props;
        return (react_1.default.createElement("div", { className: "koala_mod_tips_new" },
            react_1.default.createElement("p", null, title),
            react_1.default.createElement("div", { className: `koala_mod_tips_btnbox ${btnDirection === 'column' ? '' : 'button_direction_row'}` }, btns &&
                lodash_1.default.map(btns, (btn, index) => {
                    const { title = '', focus = false, onClick } = btn;
                    return (react_1.default.createElement("div", { key: index, className: `koala_mod_tips_btn  ${focus ? 'koala_focus' : ''}`, onClick: onClick },
                        react_1.default.createElement("span", null, title)));
                }))));
    }
}
exports.default = CardNotice;
