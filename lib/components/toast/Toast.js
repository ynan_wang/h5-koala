"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(require("react"));
const Toast = (props) => {
    const { content } = props;
    if (!content) {
        return null;
    }
    return react_1.default.createElement("div", { className: "koala_toast" }, content);
};
exports.default = Toast;
