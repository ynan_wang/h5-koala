/// <reference types="react" />
interface IProps {
    content: string;
}
declare const Toast: (props: IProps) => JSX.Element | null;
export default Toast;
