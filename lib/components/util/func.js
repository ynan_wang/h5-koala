"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getOption = exports.getRandom = exports.toPoint = exports.prevent = void 0;
const react_1 = __importDefault(require("react"));
const antd_1 = require("antd");
const { Option } = antd_1.Select;
/**
 * @param {String}  refname 需要被阻止事件的ref的名称 （要在WrappedComponent设置好）
 * @param {Array}  events 需要被阻止的事件名称
 */
exports.prevent = (props) => (WrappedComponent) => {
    let { refname, events } = props;
    return class PreventDefaultEventComponent extends react_1.default.Component {
        constructor() {
            super(...arguments);
            this.handleScroll = (event) => {
                event.preventDefault && event.preventDefault();
            };
        }
        addEvent() {
            if (this.warpRef[refname]) {
                events.forEach((event) => {
                    this.warpRef[refname].addEventListener(event, this.handleScroll);
                }, this);
            }
        }
        removeEvent() {
            if (this.warpRef[refname]) {
                events.forEach((event) => {
                    this.warpRef[refname].removeEventListener(event, this.handleScroll);
                }, this);
            }
        }
        componentDidMount() {
            this.addEvent();
        }
        componentWillUnmount() {
            this.removeEvent();
        }
        render() {
            return (react_1.default.createElement(WrappedComponent, Object.assign({ ref: (r) => {
                    this.warpRef = r;
                } }, this.props)));
        }
    };
};
// 工具类函数--string类型的百分数转成真实值
exports.toPoint = (percent) => {
    let str = Number(percent.replace('%', ''));
    str = str / 100;
    return str;
};
// 工具类函数--求随机数
function getRandom(n = '') {
    const randomNum = Math.max(1, Math.floor(Math.random() * 10));
    const numString = randomNum +
        Number(Math.random()
            .toString()
            .substr(3, 6) + Date.now()).toString(10);
    return n ? numString.slice(0, n) : numString;
}
exports.getRandom = getRandom;
// 工具函数--生成select下列表(拼接自己处理data)
function getOption(data, value, label) {
    let childrenList = [];
    if (data.length) {
        data.map((item, index) => {
            childrenList.push(react_1.default.createElement(Option, { value: item[value], key: index }, item[label]));
        });
    }
    return childrenList;
}
exports.getOption = getOption;
