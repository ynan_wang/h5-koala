"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
/* eslint-disable react/jsx-no-useless-fragment */
const react_1 = __importStar(require("react"));
const antd_1 = require("antd");
const { TextArea } = antd_1.Input;
const { Option } = antd_1.Select;
const QueryForm = (props) => {
    let keyCodes = [];
    const { colNumber = 2, queryConfig = {}, queryForm, formStyle = {}, selectStyle = {}, inputStyle = {}, textAreaStyle = {}, buttonStyle = {}, checkBoxStyle = {}, radioGroupStyle = {}, innerItemStyle, labelAlign = 'left', labelCol } = props;
    const onKeyDown = (e) => {
        keyCodes.map((item) => {
            if (e.keyCode == item.keyCode) {
                item.search();
            }
        });
    };
    react_1.useEffect(() => {
        document.addEventListener('keydown', onKeyDown);
        return () => {
            document.addEventListener('keydown', onKeyDown);
        };
    }, []);
    const getFields = () => {
        const children = [];
        for (let i = 0; i < queryConfig.length; i++) {
            let item = queryConfig[i];
            // select下拉框的值
            let options = [];
            let temp = item.data;
            temp === null || temp === void 0 ? void 0 : temp.map((item) => {
                options.push(react_1.default.createElement(Option, { value: item.id, key: item.id }, item.name));
            });
            if (item.keyCode) {
                keyCodes.push(item);
            }
            let rule = [];
            if (item.isRequire) {
                rule = item.definedRule
                    ? [...item.definedRule]
                    : [
                        {
                            required: true,
                            message: `输入${item.label}`
                        }
                    ];
            }
            children.push(react_1.default.createElement(antd_1.Col, { span: item.col ? item.col : 24 / colNumber - 2, key: i },
                react_1.default.createElement(antd_1.Form.Item, { name: item.key, label: item.isShowLabel ? item.label : undefined, labelAlign: labelAlign, labelCol: labelCol ? labelCol : undefined, style: item.style, tooltip: item.tooltip
                        ? {
                            title: item.tooltip,
                            icon: item.tooltipIcon ? item.tooltipIcon : undefined
                        }
                        : undefined, rules: rule }, item.type === 'select' ? (react_1.default.createElement(antd_1.Select, Object.assign({ style: selectStyle ? selectStyle : { width: 200 }, placeholder: `${item.label}` }, item.antdOptions), options)) : item.type === 'textArea' ? (react_1.default.createElement(TextArea, Object.assign({ style: textAreaStyle ? textAreaStyle : { width: 300 }, placeholder: `${item.label}` }, item.antdOptions))) : item.type === 'radio' ? (react_1.default.createElement(antd_1.Radio, Object.assign({ checked: true }, item.antdOptions),
                    "http \u00A0",
                    react_1.default.createElement("span", { style: { fontSize: 11, color: '#CCCCCC' } }, "\u76EE\u524D\u53EA\u652F\u6301http\u534F\u8BAE\uFF0C\u9ED8\u8BA4\u52FE\u9009"))) : item.type === 'input' ? (react_1.default.createElement(antd_1.Input, Object.assign({ placeholder: `${item.label}`, style: inputStyle ? inputStyle : { width: 300 } }, item.antdOptions))) : item.type === 'button' ? (react_1.default.createElement("div", null,
                    react_1.default.createElement(antd_1.Button, Object.assign({ style: buttonStyle ? buttonStyle : { marginRight: 20 }, onClick: item.search }, item.antdOptions), item.text))) : item.type === 'checkbox' ? (react_1.default.createElement(antd_1.Checkbox.Group, Object.assign({ options: item.data, style: checkBoxStyle }, item.antdOptions))) : item.type === 'radioGroup' ? (react_1.default.createElement(antd_1.Radio.Group, Object.assign({ options: item.data }, item.antdOptions, { style: radioGroupStyle }))) : (react_1.default.createElement(react_1.default.Fragment, null)))));
        }
        return children;
    };
    return (react_1.default.createElement(antd_1.Form, { form: queryForm, name: "queryForm", style: formStyle ? formStyle : { marginTop: 20 } },
        react_1.default.createElement(antd_1.Row, { gutter: 24 }, getFields())));
};
exports.default = QueryForm;
