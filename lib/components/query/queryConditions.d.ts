/// <reference types="react" />
interface IProps {
    queryForm: any;
    colNumber?: number;
    queryConfig: any;
    formStyle?: any;
    selectStyle?: any;
    inputStyle?: any;
    textAreaStyle?: any;
    buttonStyle?: any;
    checkBoxStyle?: any;
    radioGroupStyle?: any;
    innerItemStyle?: any;
    labelAlign?: 'left' | 'right';
    labelCol?: any;
    checkBoxList?: any;
}
declare const QueryForm: (props: IProps) => JSX.Element;
export default QueryForm;
