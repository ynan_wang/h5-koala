declare const topColResponsiveProps: {
    xs: number;
    sm: number;
    md: number;
    lg: number;
    xl: number;
    style: {
        marginBottom: number;
        paddingRight: number;
    };
};
declare const topColResponsiveProps1: {
    xs: number;
    sm: number;
    md: number;
    lg: number;
    xl: number;
    style: {
        marginBottom: number;
        marginTop: number;
        paddingLeft: number;
    };
};
declare const topColResponsiveProps2: {
    xs: number;
    sm: number;
    md: number;
    lg: number;
    xl: number;
    style: {
        marginBottom: number;
        marginTop: number;
    };
};
declare const topColResponsiveProps3: {
    xs: number;
    sm: number;
    md: number;
    lg: number;
    xl: number;
    style: {
        marginBottom: number;
        marginTop: number;
        paddingLeft: number;
        marginRight: number;
    };
};
declare const topColResponsiveProps4: {
    xs: number;
    sm: number;
    md: number;
    lg: number;
    xl: number;
    style: {
        marginBottom: number;
        paddingRight: number;
    };
};
export { topColResponsiveProps, topColResponsiveProps1, topColResponsiveProps2, topColResponsiveProps3, topColResponsiveProps4 };
