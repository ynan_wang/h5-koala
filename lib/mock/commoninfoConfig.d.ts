/// <reference types="react" />
export declare const commonInfoConfig: ({
    label: string;
    type: string;
    key: string;
    isRequire: boolean;
    isShowLabel: boolean;
    tooltip: string;
    tooltipIcon: JSX.Element;
    col: number;
    definedRule?: undefined;
    data?: undefined;
    antdOptions?: undefined;
    name?: undefined;
    text?: undefined;
} | {
    label: string;
    type: string;
    key: string;
    isRequire: boolean;
    isShowLabel: boolean;
    tooltip: string;
    col: number;
    tooltipIcon?: undefined;
    definedRule?: undefined;
    data?: undefined;
    antdOptions?: undefined;
    name?: undefined;
    text?: undefined;
} | {
    label: string;
    type: string;
    key: string;
    isRequire: boolean;
    isShowLabel: boolean;
    col: number;
    definedRule: {
        pattern: RegExp;
        message: string;
    }[];
    tooltip?: undefined;
    tooltipIcon?: undefined;
    data?: undefined;
    antdOptions?: undefined;
    name?: undefined;
    text?: undefined;
} | {
    label: string;
    type: string;
    isRequire: boolean;
    isShowLabel: boolean;
    key: string;
    col: number;
    data: {
        label: string;
        value: string;
    }[];
    tooltip?: undefined;
    tooltipIcon?: undefined;
    definedRule?: undefined;
    antdOptions?: undefined;
    name?: undefined;
    text?: undefined;
} | {
    label: string;
    type: string;
    key: string;
    col: number;
    isRequire: boolean;
    isShowLabel: boolean;
    tooltip?: undefined;
    tooltipIcon?: undefined;
    definedRule?: undefined;
    data?: undefined;
    antdOptions?: undefined;
    name?: undefined;
    text?: undefined;
} | {
    label: string;
    type: string;
    key: string;
    antdOptions: {
        autoSize: {
            minRows: number;
            maxRows: number;
        };
        type?: undefined;
    };
    col: number;
    isRequire: boolean;
    isShowLabel: boolean;
    tooltip?: undefined;
    tooltipIcon?: undefined;
    definedRule?: undefined;
    data?: undefined;
    name?: undefined;
    text?: undefined;
} | {
    key: string;
    name: string;
    type: string;
    text: string;
    antdOptions: {
        type: string;
        autoSize?: undefined;
    };
    col: number;
    label?: undefined;
    isRequire?: undefined;
    isShowLabel?: undefined;
    tooltip?: undefined;
    tooltipIcon?: undefined;
    definedRule?: undefined;
    data?: undefined;
})[];
