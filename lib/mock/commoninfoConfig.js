"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.commonInfoConfig = void 0;
const react_1 = __importDefault(require("react"));
const icons_1 = require("@ant-design/icons");
exports.commonInfoConfig = [
    {
        label: '业务线信息',
        type: 'select',
        key: 'appList',
        isRequire: true,
        isShowLabel: true,
        tooltip: '哈哈哈哈哈, 很高兴遇到你',
        tooltipIcon: react_1.default.createElement(icons_1.InfoCircleOutlined, null),
        col: 6
    },
    {
        label: '埋点标识符',
        type: 'input',
        key: 'sign',
        isRequire: true,
        isShowLabel: true,
        tooltip: '老邓',
        col: 6
    },
    {
        label: '埋点名称',
        type: 'input',
        key: 'name',
        isRequire: true,
        isShowLabel: true,
        col: 6,
        definedRule: [{ pattern: /^[A-Za-z0-9_]+$/, message: '只能字母、数字、下划线' }]
    },
    {
        label: '上报终端类',
        type: 'checkbox',
        isRequire: true,
        isShowLabel: true,
        key: 'platform',
        col: 24,
        data: [
            { label: 'Apple', value: 'Apple' },
            { label: 'Pear', value: 'Pear' },
            { label: 'Orange', value: 'Orange' },
            { label: 'Apple1', value: 'Apple1' },
            { label: 'Pear1', value: 'Pear1' },
            { label: 'Orange1', value: 'Orange1' },
            { label: 'Apple2', value: 'Apple2' },
            { label: 'Pear2', value: 'Pear2' }
        ]
    },
    {
        label: '埋点子事件',
        type: 'radioGroup',
        isRequire: true,
        isShowLabel: true,
        key: 'platform',
        col: 24,
        data: [
            { label: '有', value: '1' },
            { label: '无', value: '2' }
        ]
    },
    {
        label: '交互类型',
        type: 'select',
        key: 'interaction',
        col: 8,
        isRequire: true,
        isShowLabel: true
    },
    {
        label: '上报时机/埋点位置说明',
        type: 'textArea',
        key: 'interaction',
        antdOptions: {
            autoSize: { minRows: 1, maxRows: 4 }
        },
        col: 9,
        isRequire: false,
        isShowLabel: false
    },
    {
        key: 'action',
        name: '按钮',
        type: 'button',
        text: '添加参数',
        antdOptions: {
            type: 'primary'
        },
        col: 20
    }
];
