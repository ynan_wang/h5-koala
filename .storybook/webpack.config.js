const path = require('path')

module.exports = ({ config, mode }) => {
    config.module.rules.push({
        test: /\.scss$/,
        loaders: ['style-loader', 'css-loader', 'sass-loader'],
        include: path.resolve(__dirname, '../')
    },
    {
        test: /\.(ts|tsx)$/,
        loader: require.resolve('babel-loader'),
        options: {
            presets: [['react-app', { flow: false, typescript: true }]],
            plugins: [
                ["import", { libraryName: "antd", style: "css" }] 
              ]
        },
    })
    config.resolve.alias = {
        '@': path.resolve(__dirname, './src')
    }
    config.resolve.extensions.push('.ts', '.tsx');
    return config
}
