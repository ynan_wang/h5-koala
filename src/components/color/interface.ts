import { defaultColors, gradientColors, colors, KColor } from './type'
export type IGradientColorIndex = keyof typeof gradientColors
export type IGradientColorValue = typeof gradientColors[IGradientColorIndex]
export type IColorIndex = keyof typeof defaultColors
export type IColorValue = typeof defaultColors[IColorIndex]
export type IColorsIndex = keyof typeof colors

export type IKColorIndex = keyof typeof KColor
export type IKColorValue = typeof KColor
