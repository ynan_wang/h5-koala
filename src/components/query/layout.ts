const topColResponsiveProps = {
    xs: 24,
    sm: 24,
    md: 24,
    lg: 24,
    xl: 24,
    style: { marginBottom: 15, paddingRight: 0 }
}
const topColResponsiveProps1 = {
    xs: 8,
    sm: 8,
    md: 8,
    lg: 8,
    xl: 8,
    style: { marginBottom: 15, marginTop: 15, paddingLeft: 15 }
}
const topColResponsiveProps2 = {
    xs: 16,
    sm: 16,
    md: 16,
    lg: 16,
    xl: 16,
    style: { marginBottom: 15, marginTop: 15 }
}

const topColResponsiveProps3 = {
    xs: 3,
    sm: 3,
    md: 3,
    lg: 3,
    xl: 3,
    style: { marginBottom: 15, marginTop: 15, paddingLeft: 15, marginRight: 10 }
}
const topColResponsiveProps4 = {
    xs: 20,
    sm: 20,
    md: 20,
    lg: 20,
    xl: 20,
    style: { marginBottom: 15, paddingRight: 0 }
}

export {
    topColResponsiveProps,
    topColResponsiveProps1,
    topColResponsiveProps2,
    topColResponsiveProps3,
    topColResponsiveProps4
}
