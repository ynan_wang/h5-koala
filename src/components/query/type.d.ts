export interface dropListInterface {
    id: number
    name: string
}

export interface queryInfoConfigInterface extends dropListInterface {
    id: string
    name: string
    type: string
}

export interface apiListInterface {
    id: number
    sign: string
    name: string
    datasource: string
    apps: string
    status: number
    creator: string
    createTime: string
    updator?: string
    updateTime: string
    editFlag?: number
}

export interface filterProps {
    status?: number
    name?: string
    appId?: number
    datasourceId?: number
    creator?: string
}

export interface macroItemsInterface {
    key?: string
    filterSign?: string
    valueType?: string
    description?: string
    valueSign?: string
    filterOn?: string
    required?: boolean
}

export interface apiDetailInterface {
    id?: number
    sign?: string
    name?: string
    description?: string
    datasourceId?: number
    datasource?: string
    appList?: number[]
    apps?: string
    sqlTemplate?: string
    macroList?: {
        placeSign?: string
        macroItems?: macroItemsInterface[]
    }[]
    columnList?: {
        name: string
        alias: string
        valueType: number
        defaultValue: string
        function: () => void
    }[]
    creator?: string
    createTime?: string
    updator?: string
    updateTime?: string
}

// export {
//     dropListInterface,
//     apiListInterface,
//     filterProps,
//     queryInfoConfigInterface,
//     apiDetailInterface,
//     macroItemsInterface
// }
