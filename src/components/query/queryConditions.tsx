/* eslint-disable react/jsx-no-useless-fragment */
import React, { useEffect } from 'react'
import { Form, Row, Col, Input, Select, Radio, Button, Checkbox } from 'antd'
const { TextArea } = Input
const { Option } = Select

interface IProps {
    queryForm: any
    colNumber?: number
    queryConfig: any
    formStyle?: any
    selectStyle?: any
    inputStyle?: any
    textAreaStyle?: any
    buttonStyle?: any
    checkBoxStyle?: any
    radioGroupStyle?: any
    innerItemStyle?: any
    labelAlign?: 'left' | 'right'
    labelCol?: any
    checkBoxList?: any
}

interface itemInterface {
    data?: [{ id: number; name: string }] // select下拉框的option
    col?: number // 该item占据col
    key?: string
    isShowLabel?: boolean // 是否展示标签
    label?: string // 标签文案
    style?: any // item样式
    tooltip?: string // formItem tooltip
    tooltipIcon?: any
    isRequire?: boolean
    definedRule?: any[]
    type: 'select' | 'input' | 'radio' | 'textArea' | 'checkbox' | 'radioGroup' | 'button'
    antdOptions?: any // item的antd属性
    search?: () => void // button的onlick
    text?: string // button文案
    keyCode?: number // 配置快捷键
}

const QueryForm = (props: IProps) => {
    let keyCodes: any = []
    const {
        colNumber = 2,
        queryConfig = {},
        queryForm,
        formStyle = {},
        selectStyle = {},
        inputStyle = {},
        textAreaStyle = {},
        buttonStyle = {},
        checkBoxStyle = {},
        radioGroupStyle = {},
        innerItemStyle,
        labelAlign = 'left',
        labelCol
    } = props

    const onKeyDown = (e: any) => {
        keyCodes.map((item: any) => {
            if (e.keyCode == item.keyCode) {
                item.search()
            }
        })
    }

    useEffect(() => {
        document.addEventListener('keydown', onKeyDown)
        return () => {
            document.addEventListener('keydown', onKeyDown)
        }
    }, [])

    const getFields = () => {
        const children = []
        for (let i = 0; i < queryConfig.length; i++) {
            let item: itemInterface = queryConfig[i]
            // select下拉框的值
            let options: any = []
            let temp: any = item.data
            temp?.map((item: any) => {
                options.push(
                    <Option value={item.id} key={item.id}>
                        {item.name}
                    </Option>
                )
            })
            if (item.keyCode) {
                keyCodes.push(item)
            }
            let rule: any = []
            if (item.isRequire) {
                rule = item.definedRule
                    ? [...item.definedRule]
                    : [
                          {
                              required: true,
                              message: `输入${item.label}`
                          }
                      ]
            }

            children.push(
                <Col span={item.col ? item.col : 24 / colNumber - 2} key={i}>
                    <Form.Item
                        name={item.key}
                        label={item.isShowLabel ? item.label : undefined}
                        labelAlign={labelAlign}
                        labelCol={labelCol ? labelCol : undefined}
                        style={item.style}
                        tooltip={
                            item.tooltip
                                ? {
                                      title: item.tooltip,
                                      icon: item.tooltipIcon ? item.tooltipIcon : undefined
                                  }
                                : undefined
                        }
                        rules={rule}
                    >
                        {item.type === 'select' ? (
                            <Select
                                style={selectStyle ? selectStyle : { width: 200 }}
                                placeholder={`${item.label}`}
                                {...item.antdOptions}
                            >
                                {options}
                            </Select>
                        ) : item.type === 'textArea' ? (
                            <TextArea
                                style={textAreaStyle ? textAreaStyle : { width: 300 }}
                                placeholder={`${item.label}`}
                                {...item.antdOptions}
                            />
                        ) : item.type === 'radio' ? (
                            <Radio checked={true} {...item.antdOptions}>
                                http &nbsp;
                                <span style={{ fontSize: 11, color: '#CCCCCC' }}>
                                    目前只支持http协议，默认勾选
                                </span>
                            </Radio>
                        ) : item.type === 'input' ? (
                            <Input
                                placeholder={`${item.label}`}
                                style={inputStyle ? inputStyle : { width: 300 }}
                                {...item.antdOptions}
                            />
                        ) : item.type === 'button' ? (
                            <div>
                                <Button
                                    style={buttonStyle ? buttonStyle : { marginRight: 20 }}
                                    onClick={item.search}
                                    {...item.antdOptions}
                                >
                                    {item.text}
                                </Button>
                            </div>
                        ) : item.type === 'checkbox' ? (
                            <Checkbox.Group
                                options={item.data}
                                style={checkBoxStyle}
                                {...item.antdOptions}
                            />
                        ) : item.type === 'radioGroup' ? (
                            <Radio.Group
                                options={item.data}
                                {...item.antdOptions}
                                style={radioGroupStyle}
                            />
                        ) : (
                            <></>
                        )}
                    </Form.Item>
                </Col>
            )
        }
        return children
    }
    return (
        <Form form={queryForm} name="queryForm" style={formStyle ? formStyle : { marginTop: 20 }}>
            <Row gutter={24}>{getFields()}</Row>
        </Form>
    )
}

export default QueryForm
