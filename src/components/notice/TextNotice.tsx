import React, { Component, useEffect, useState } from 'react'
import _ from 'lodash'
import classnames from 'classnames'

interface IProps {
    color: string
    messageArray: {
        message: string
        onShowDetail?: () => void
        isLimitRow?: boolean // 默认为true 文本截断，单文本三行截断，成组文本，没条单行截断
        detailedText?: string
    }[]
    onJump?: () => void
    onClose?: () => void
    button?: {
        title: string
        onClick: () => void
    }
    leftIconType?: 'Dot' | 'Information'
    backgroundColor?: string
    padding?: {
        right?: string
        left?: string
        top?: string
        bottom?: string
    }
    margin?: {
        right?: string
        left?: string
        top?: string
        bottom?: string
    }
    borderRadius?: {
        topLeft?: string
        topRight?: string
        bottomLeft?: string
        bottomRight?: string
    }
}

interface IState {
    isShowArrow: boolean
    isMoreLines: boolean
}
export default class TextNotice extends Component<IProps, IState> {
    constructor(props: IProps) {
        super(props)
        this.state = {
            isShowArrow: false,
            isMoreLines: false // 有截断要求但是没超出行数
        }
    }
    componentDidMount() {
        const { messageArray = [] } = this.props
        let isShowdetailed = _.some(messageArray, (message) => _.isFunction(message.onShowDetail))
        // 单文本，若有截断需求，只有超出三行才截断
        // 若为多文本，且有截断需求,单条超过一行就截断
        const koala_txt_arr = document.getElementsByClassName('koala_txt') || []
        let textHeight = 0
        let virtualHeight = 0
        _.map(koala_txt_arr, (ele, index) => {
            if (index % 2 === 0) {
                textHeight += koala_txt_arr[index].clientHeight
            } else {
                virtualHeight += koala_txt_arr[index].clientHeight
            }
        })
        if (textHeight !== virtualHeight) {
            if (!isShowdetailed) {
                this.setState({ isShowArrow: true })
            }
            this.setState({ isMoreLines: true })
        }
    }
    render() {
        const {
            color = '#FF7700',
            messageArray = [],
            onJump,
            onClose,
            button = {},
            leftIconType,
            backgroundColor = '#FFFFFF',
            padding = {},
            margin = {},
            borderRadius = {}
        } = this.props
        let isMoreTips = false
        if (messageArray.length > 1) {
            isMoreTips = true
        }
        return (
            <div
                className="koala_ellipsisTxt"
                style={{
                    color: color,
                    backgroundColor: backgroundColor,
                    paddingLeft: padding.left,
                    paddingRight: padding.right,
                    paddingTop: padding.top,
                    paddingBottom: padding.bottom,
                    marginLeft: margin.left,
                    marginRight: margin.right,
                    marginTop: margin.top,
                    marginBottom: margin.bottom,
                    borderTopLeftRadius: borderRadius.topLeft,
                    borderTopRightRadius: borderRadius.topRight,
                    borderBottomLeftRadius: borderRadius.bottomLeft,
                    borderBottomRightRadius: borderRadius.bottomRight
                }}
            >
                <div className="koala_tips_box">
                    {messageArray &&
                        _.map(messageArray, (messageObj: any, index: number) => {
                            let {
                                message = '',
                                onShowDetail,
                                isLimitRow = true,
                                detailedText = '查看详情'
                            } = messageObj
                            let isShowDetail = false
                            if (typeof onShowDetail === 'function') {
                                isShowDetail = true
                            }

                            return (
                                <div key={index} className="koala_risk_tips">
                                    {leftIconType === 'Dot' && (
                                        <i
                                            className="koala_hasLeftPoint"
                                            style={{ background: color }}
                                        />
                                    )}
                                    {leftIconType === 'Information' && (
                                        <i className="Koala_infoIcon" />
                                    )}
                                    <p
                                        className="koala_txt"
                                        style={{
                                            WebkitLineClamp: isLimitRow
                                                ? isMoreTips
                                                    ? 1
                                                    : 3
                                                : 'none'
                                        }}
                                        id="textHeight"
                                    >
                                        <span>{message}</span>
                                        {!isLimitRow ||
                                            (isLimitRow &&
                                                !this.state.isMoreLines &&
                                                isShowDetail && (
                                                    <span
                                                        className="koala_showDetail"
                                                        onClick={onShowDetail}
                                                    >
                                                        {detailedText}
                                                    </span>
                                                ))}
                                    </p>
                                    {/* 对照组 */}
                                    <p
                                        className="koala_txt"
                                        style={{ position: 'absolute', top: '-100000000px' }}
                                        id="textHeightVirtual"
                                    >
                                        <span>{message}</span>
                                        {isMoreTips && isShowDetail && (
                                            <span
                                                className="koala_showDetail"
                                                onClick={onShowDetail}
                                            >
                                                {detailedText}
                                            </span>
                                        )}
                                    </p>
                                    {isLimitRow && this.state.isMoreLines && isShowDetail && (
                                        <span
                                            className="koala_showDetail_more"
                                            onClick={onShowDetail}
                                        >
                                            {detailedText}
                                        </span>
                                    )}
                                </div>
                            )
                        })}
                </div>
                <div className="koala_operation">
                    {!_.isEmpty(button) && (
                        <a
                            className="button"
                            onClick={_.get(button, 'onClick')}
                            style={{ color: color }}
                        >
                            {_.get(button, 'title')}
                        </a>
                    )}
                    {this.state.isShowArrow && _.isFunction(onJump) && (
                        <a className="iconfont_jump" onClick={onJump} style={{ color: color }} />
                    )}
                    {_.isFunction(onClose) && (
                        <a className="iconfont_close" onClick={onClose} style={{ color: color }} />
                    )}
                </div>
            </div>
        )
    }
}
