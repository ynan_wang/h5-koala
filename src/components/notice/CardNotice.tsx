import React, { Component } from 'react'
import _ from 'lodash'

interface IProps {
    title: string
    btnDirection?: 'row' | 'column'
    btns: {
        title: string
        focus: boolean
        onClick: () => void
    }[]
}

export default class CardNotice extends Component<IProps> {
    render() {
        const { title = '', btnDirection = 'column', btns = [] } = this.props

        return (
            <div className="koala_mod_tips_new">
                <p>{title}</p>
                <div
                    className={`koala_mod_tips_btnbox ${
                        btnDirection === 'column' ? '' : 'button_direction_row'
                    }`}
                >
                    {btns &&
                        _.map(btns, (btn: any, index: number) => {
                            const { title = '', focus = false, onClick } = btn
                            return (
                                <div
                                    key={index}
                                    className={`koala_mod_tips_btn  ${focus ? 'koala_focus' : ''}`}
                                    onClick={onClick}
                                >
                                    <span>{title}</span>
                                </div>
                            )
                        })}
                </div>
            </div>
        )
    }
}
