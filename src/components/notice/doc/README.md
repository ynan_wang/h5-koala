# 信息提示

# 单多文本提示A/B 组件使用

## 代码
```javascript

const messageArray=[{message:'文本文本文本本文本文文本文本文本本文本文文本文本文本本文本文文本文本文本本文本文文本文本文本本文本文'}]

const moreMessageArray=[{message:'本文本文本文本文本本文本文本。',onShowDetail:()=>alert("点击查看详情详情")},
{message:'文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本'},
{message:'文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本',detailedText:"详情",onShowDetail:()=>alert("点击详情")}]
const button={title:'次要按钮',onClick:()=>{alert('点击次要按钮')}}
<TextNotice
    leftIconType="Information" 
    color='#666666' 
    messageArray={messageArray} 
    button={title:'次要按钮',onClick:()=>{alert('点击次要按钮')}}
    onClose={()=>alert('点击关闭按钮')}
/>

<TextNotice 
    color ='#FF7700' 
    messageArray={messageArrayMore} 
    onClose={()=>alert('点击关闭按钮')} 
    leftIconType="Information" 
    backgroundColor="#FFF8F2"/>
```

## API
| 属性         | 说明         | 类型                    | 默认值   | 必选 |
| ------------ | ------------ | ----------------------- | -------- | ---- |
| leftIconType        | 是否有左边的图标 |  "Dot" 或"Information"    |     | 否        | 
| color         | 字体颜色         | string                |          | 是   |
| messageArray         | 文本提示信息 |  [array](#messageArray)    |          |是    |
| onJump | 是和否支持跳转 | function       |   | 否   |
| onClose | 是和否支持关闭 | function       |   | 否   |
| button | 是否支持次要按钮 | [object](#button)       |   | 否   |
| backgroundColor | 背景颜色 | string      |  "#FFFFFF"  | 否  |
| padding | 内边距 |   [object](#padding)     |    | 否  |
| margin | 外边距 |   [object](#margin)     |    | 否  |
| borderRadius | 圆角 |   [object](#borderRadius)     |    | 否  |

### messageArray

```javascript
    messageArray : {
        message string 要展示的信息 
        onShowDetail functoin 是否支持详情
        isLimitRow?: boolean  是否限制行数（默认限制行数 单文本限制三行，成组成本，每条限制一行截断）
        detailedText?:string  自定义详情文本（默认"查看详情"）
    }[]
```

### button

```javascript
    button : {
        title: string  按钮文本
        onClick: () => void 点击按钮
    }
```

### padding

```javascript
    padding?: {
        left?: string 左边距
        right?: string 右边距
        top?: string 上边距
        bottom?: string  下边距
    }
```
### margin

```javascript
    margin?: {
        left?: string 左边距
        right?: string 右边距
        top?: string 上边距
        bottom?: string  下边距
    }
```
### borderRadius

```javascript
    borderRadius?: {
        topLeft?:string, 
        topRight?:string, 
        bottomLeft?:string, 
        bottomRight?:string  
    }
```



## 示例
![单条文本 图片](./picture/singleText.png)
![多条文本 图片](./picture/moreTextTips.png)


# 卡片C1 组件使用

## 代码

```javascript
   
    const btns=[
        {title:'按钮文字最多十八个字',focus:false,onClick:()=>alert('点击按钮')},
        {title:'焦点按钮强提示',focus:true,onClick:()=>alert('点击按钮')}
    ]

    <CardNotice
        title="资源变动提示"
        btns={btns}
        btnDirection="row"
    />
}
```

## API
| 属性         | 说明         | 类型                    | 默认值   | 必选 |
| ------------ | ------------ | ----------------------- | -------- | ---- |
| title        | 标题 | string                  |          | 是   |
| btnDirection         | 字体颜色         | ‘row’ 或‘column’              |  ‘column’       | 否   |
| btns         | 按钮 |  [array](#btns)    |          |是    |

### btns
```javascript
    btns : {
        title string 按钮名称
        focus boolean 是否选中
        onClick function 点击事件
    }[]
```

## 示例
![单条文本 图片](./picture/cardNotice.png)

# 卡片C2-3 组件使用

## 代码
```javascript
    const color="#FF7700";
    const messageArrayPaneL=[
        {message:"提示话术提示话术提示话术提示话话术提示话术"},
        {message:"提示话术提示话术提示话术提示话话术提示话术提示话术提示话术提示话术提示话话术提示话术提示话术提示话术提示话术提示话话术提示话术"},
        {
            message:"提示话术提示话术提示话术提示话术提示话术提示话术提示话术提示话术提示话术提示话术提示话术提示话术提示话术提示话术提示话术提示话术提示话术提示话术",
            onClose:()=>alert('关闭按钮')
        },
        {
            message:"提示话术提示话术提示话术提示话术提示话术提示话术提示话术提示话术提示话术提示话术提示话术提示话术提示话术提示话术提示话术提示话术提示话术提示话术",
            onClose:()=>alert('关闭按钮'),
            button:{title:"一键替换",onClick:()=>alert('一键替换')}
        },
        {
            message:"提示话术提示话术提示话术提示话术提示话术提示话术提示话术提示话术提示话术",
            button:{title:"一键替换",onClick:()=>alert('一键替换')}
        }
    ]

    <PanelNotice color={color} messageArray={messageArrayPaneL}/>
```

## API
| 属性         | 说明         | 类型                    | 默认值   | 必选 |
| ------------ | ------------ | ----------------------- | -------- | ---- |
| color        | 字体颜色 | string                  |          | 是   |
| messageArray         | 面板提示信息        | [Array](#messageArray1)            |       | 是   |

### messageArray1

```javascript
    messageArray : {
        message strign面板信息
        onClose? function是否支持关闭
        button?<{ title, onClick }> } 按钮名称及事件
        title: string
        onClick: () => {}
    }[]
```

## 示例
![面板提示信息 图片](./picture/cardNotice.png)