// 代理函数
function validator(target, validator, errorMsg) {
    return new Proxy(target, {
        _validator: validator,
        set(target, key, value, proxy) {
            let errMsg = errorMsg
            let va = this._validator[key]
            if (va(value)) {
                return Reflect.set(target, key, value, proxy)
            } else {
                alert(errMsg[key])
                return true
            }
        }
    })
}

// 校验函数
// function vaildatorFun(values,validators, errorMes){
//     let vali = validator({}, validators, errorMes)
//     let validatorNext = function*() {
//         yield (vali.menuPathNeed = menuPath)
//         yield (vali.menuPathValid = menuPath)
//         yield (vali.menuLevel = menuPath)
//         yield (vali.kanBanName = kanBanName)
//         return true
//     }
//     let validatorfun = validatorNext()
//     validatorfun.next()
//     vali.menuPathNeed && validatorfun.next()
//     vali.menuPathValid && validatorfun.next()
//     vali.menuLevel && validatorfun.next()
//     vali.kanBanName && validatorfun.next()

//     // 根据对象vali的value是否为''判断是否能执行次下去
// }
