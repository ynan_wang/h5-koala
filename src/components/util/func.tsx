import React from 'react'
import { Select } from 'antd'
const { Option } = Select
interface IProps {
    refname: string
    events: string[]
}
/**
 * @param {String}  refname 需要被阻止事件的ref的名称 （要在WrappedComponent设置好）
 * @param {Array}  events 需要被阻止的事件名称
 */
export const prevent = (props: IProps) => (WrappedComponent: any) => {
    let { refname, events } = props
    return class PreventDefaultEventComponent extends React.Component {
        warpRef: any
        addEvent() {
            if (this.warpRef[refname]) {
                events.forEach((event) => {
                    this.warpRef[refname].addEventListener(event, this.handleScroll)
                }, this)
            }
        }
        removeEvent() {
            if (this.warpRef[refname]) {
                events.forEach((event) => {
                    this.warpRef[refname].removeEventListener(event, this.handleScroll)
                }, this)
            }
        }
        handleScroll = (event: any) => {
            event.preventDefault && event.preventDefault()
        }
        componentDidMount() {
            this.addEvent()
        }
        componentWillUnmount() {
            this.removeEvent()
        }
        render() {
            return (
                <WrappedComponent
                    ref={(r: any) => {
                        this.warpRef = r
                    }}
                    {...this.props}
                />
            )
        }
    }
}

// 工具类函数--string类型的百分数转成真实值
export const toPoint = (percent: string) => {
    let str = Number(percent.replace('%', ''))
    str = str / 100
    return str
}

// 工具类函数--求随机数
export function getRandom(n: any = '') {
    const randomNum = Math.max(1, Math.floor(Math.random() * 10))
    const numString =
        randomNum +
        Number(
            Math.random()
                .toString()
                .substr(3, 6) + Date.now()
        ).toString(10)
    return n ? numString.slice(0, n) : numString
}

// 工具函数--生成select下列表(拼接自己处理data)
export function getOption(data: any[], value: any, label: any) {
    let childrenList: any = []
    if (data.length) {
        data.map((item: any, index: number) => {
            childrenList.push(
                <Option value={item[value]} key={index}>
                    {item[label]}
                </Option>
            )
        })
    }
    return childrenList
}
