import React from 'react'

interface IProps {
    content: string
}
const Toast = (props: IProps) => {
    const { content } = props
    if (!content) {
        return null
    }
    return <div className="koala_toast">{content}</div>
}

export default Toast
